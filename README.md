
Program for detecting and switcing between "tablet" mode and "laptop" mode for Tablet PCs
with touch screens that can rotate.

This program is for screens that send a "toggle mode" keyboard event when changing modes.

Requirements
------------

The `xinput` program is needes to run.

Build this as a normal rust project, by installing rust table from rustup and running `cargo build`.

Configuration
-------------

This program takes a TOML formatted configuration file as its sole argument. Example file with all fields:

```toml
# Name of input device, according to `xinput list`
input_device = "Asus WMI hotkeys"
# Key ID, according to `xinput test`, to listen for. If omitted, any key event will trigger a switch.
input_key_id = 157
# Commands to run when changing to "tablet mode". This example disables the keyboard and touchpad mouse.
to_tablet_commands = [
	"xinput disable \"ELAN1300:00 04F3:3032 Touchpad\"",
	"xinput disable \"AT Translated Set 2 keyboard\"",
]
# Commands to run when changing to "laptop mode". This example re-enables the above.
to_laptop_commands = [
	"xinput enable \"ELAN1300:00 04F3:3032 Touchpad\"",
	"xinput enable \"AT Translated Set 2 keyboard\"",
]

# By default, the script assumes the device is in laptop mode when it starts
# (since the 'keyboard' sends toggle events, we can't get a 'state').
# Set this to true to assume the device is in tablet mode instead.
#start_in_tablet_mode = false

# If set to true, run the commands for the assumed mode when the program starts.
#run_commands_on_start = true
```

Finding your device
-------------------

First run `xinput list` to list all your devices. Look through the keyboards section for a device that looks like
it may send the events (probably not a real keyboard, or USB camera or something).

Execute `xinput test <device_name>` and rotate your screen to tablet mode and vice versa.
If you see a key event in the console when it happens, you've found the device.

If you can't find anything, then your laptop may have a different mechanism for notifying the OS
about modes. In that case, this program will not work for you.

Note that the device IDs change from boot to boot, so you should use the device name instead.

Autorunning
-----------

Use a method appropriate for your setup; for example, the autorun apps setting of your desktop environment.

Wayland
-------

Not supported, at least not yet.

Wayland has no standard mechanism for enabling/disabling inputs anyway (the main use case for this program).
