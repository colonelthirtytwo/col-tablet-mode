use serde::Deserialize;
use std::{
	fs,
	io::{BufRead, BufReader},
	mem::drop,
	path::PathBuf,
	process::{ChildStdout, Command, Stdio},
};
use structopt::StructOpt;
use toml;

#[derive(Debug, StructOpt)]
pub struct Args {
	/// Configuration file in toml format.
	pub config: PathBuf,
}

#[derive(Debug, Deserialize)]
pub struct Config {
	/// Name (from xinput list) of device to read from
	pub input_device: String,
	/// Key from device to read from, or `None` to switch on any key
	#[serde(default)]
	pub input_key_id: Option<u32>,
	/// Commands to run when switching to tablet mode
	pub to_tablet_commands: Vec<String>,
	/// Commands to run when switching to laptop mode
	pub to_laptop_commands: Vec<String>,
	/// Whether to run the `to_laptop/tablet_commands` when the program starts
	#[serde(default)]
	pub run_commands_on_start: bool,
	/// Start assuming we are in tablet mode rather than laptop mode
	#[serde(default)]
	pub start_in_tablet_mode: bool,
}

fn main() -> Result<(), String> {
	let args = Args::from_args();

	let config_bytes =
		fs::read(&args.config).map_err(|e| format!("Could not read {:?}: {}", args.config, e))?;
	let config = toml::from_slice::<Config>(&config_bytes)
		.map_err(|e| format!("Could not parse {:?}: {}", args.config, e))?;
	drop(config_bytes);

	let mut is_tablet_mode = config.start_in_tablet_mode;

	if config.run_commands_on_start {
		println!("Running startup commands");
		if is_tablet_mode {
			exec_commands(&config.to_tablet_commands);
		} else {
			exec_commands(&config.to_laptop_commands);
		}
	}

	for res in ButtonIter::new(&config.input_device)? {
		let (keycode, is_down) = match res {
			Ok(v) => v,
			Err(e) => {
				return Err(e);
			}
		};

		if !is_down {
			continue;
		}
		if config.input_key_id.is_some() && config.input_key_id == Some(keycode) {
			if is_tablet_mode {
				println!("Switching to laptop mode");
				exec_commands(&config.to_laptop_commands);
			} else {
				println!("Switching to tablet mode");
				exec_commands(&config.to_tablet_commands);
			}
			is_tablet_mode = !is_tablet_mode;
		}
	}
	Ok(())
}

fn exec_commands<S: AsRef<str>>(cmds: &[S]) {
	for cmdstr in cmds.iter() {
		let res = Command::new("/bin/sh")
			.arg("-c")
			.arg(cmdstr.as_ref())
			.stdin(Stdio::inherit())
			.stdout(Stdio::inherit())
			.stderr(Stdio::inherit())
			.output();
		let code = match res {
			Ok(out) => out.status,
			Err(e) => {
				eprintln!("Could not execute /bin/sh: {}", e);
				continue;
			}
		};

		if !code.success() {
			eprintln!("Command {:?} failed with code {}", cmdstr.as_ref(), code);
		}
	}
}

struct ButtonIter {
	stdout: BufReader<ChildStdout>,
	re: regex::Regex,
	buf: String,
}
impl ButtonIter {
	pub fn new(name: &str) -> Result<Self, String> {
		let mut child = Command::new("xinput")
			.arg("test")
			.arg(name)
			.stdin(Stdio::null())
			.stdout(Stdio::piped())
			.stderr(Stdio::inherit())
			.spawn()
			.map_err(|e| format!("Could not execute `xinput test`: {}", e))?;
		Ok(Self {
			stdout: BufReader::new(child.stdout.take().unwrap()),
			buf: String::new(),
			re: regex::Regex::new(r"^key\s+([^ ]+)\s+(\d+)\s*$").unwrap(),
		})
	}
}
impl Iterator for ButtonIter {
	type Item = Result<(u32, bool), String>;
	fn next(&mut self) -> Option<Self::Item> {
		loop {
			self.buf.clear();
			let num_read = match self.stdout.read_line(&mut self.buf) {
				Ok(v) => v,
				Err(e) => {
					return Some(Err(format!("Could not read from xinput: {}", e)));
				}
			};
			if num_read == 0 {
				return None;
			}
			if let Some(m) = self.re.captures(&self.buf) {
				return Some(Ok((
					m.get(2).unwrap().as_str().parse().unwrap(),
					m.get(1).unwrap().as_str() == "press",
				)));
			}
		}
	}
}
